package game;

import interfaces.CharacterFactory;
import interfaces.Platform;
import objects.Block;
import characters.*;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.util.ArrayList;
import java.util.List;

import javax.swing.*;

import objects.GameObjectImpl;
import objects.Piece;
import objects.Tunnel;
import utils.Res;
import utils.Utils;

@SuppressWarnings("serial")
public class PlatformImpl extends JPanel implements Platform {

    private static final int MARIO_X = 300;
    private static final int MARIO_Y = 245;
    private static final int MUSHROOM_X = 800;
    private static final int MUSHROOM_Y = 263;
    private static final int TURTLE_X = 950;
    private static final int TURTLE_Y = 243;

    private static final int MARIO_FREQUENCY = 25;
    private static final int MUSHROOM_FREQUENCY = 45;
    private static final int TURTLE_FREQUENCY = 45;
    private static final int MUSHROOM_DEAD_OFFSET_Y = 20;
    private static final int TURTLE_DEAD_OFFSET_Y = 30;
    private static final int FLAG_X_POS = 4650;
    private static final int CASTLE_X_POS = 4850;
    private static final int FLAG_Y_POS = 115;
    private static final int CASTLE_Y_POS = 145;
    private Image imgBackground1;
    private Image imgBackground2;
    private Image castle;
    private Image start;

    private int background1PosX;
    private int background2PosX;
    private int mov;
    private int xPos;
    private int floorOffsetY;
    private int heightLimit;

    private static final int COORD_Y_TUNNEL = 230;
    private static final int[] COORDS_Y_BLOCK = {180,180,170,160,180,160,180,160,140,170,200,210};
    private static final int[] COORDS_Y_PIECE = {145,140,95,40,145,145,135,125,145,40};
    private static final int[] COORDS_X_TUNNEL = {600,1000,1600,1900,2500,3000,3800,4500};
    private static final int[] COORDS_X_BLOCK = {400,1200,1270,1340,2000,2600,2650,3500,3550,3550,4000,4200,4300};
    private static final int[] COORDS_X_PIECE = {402,1202,1272,1342,1650,2650,3000,3400,4200,4600};

    private CharacterFactory characterFactory = new CharacterFactoryImpl();

    private Mario mario;
    private Mushroom mushroom;
    private Turtle turtle;

    private List<Tunnel> tunnelList = new ArrayList<>();
    private List<Block> blockList = new ArrayList<>();
    private List<Piece> pieceList = new ArrayList<>();

    private Image imgFlag;
    private Image imgCastle;

    private ArrayList<GameObjectImpl> objects;
    private ArrayList<Piece> pieces;

    private static PlatformImpl PLATFORM = null;

    public static PlatformImpl getPlatform() {
        if (PLATFORM == null){
            synchronized (PlatformImpl.class){
                if (PLATFORM == null){
                    PLATFORM = new PlatformImpl();
                }
            }
        }
        return PLATFORM;
    }

    private PlatformImpl() {
        super();
        this.background1PosX = -50;
        this.background2PosX = 750;
        this.mov = 0;
        this.xPos = -1;
        this.floorOffsetY = 293;
        this.heightLimit = 0;
        this.imgBackground1 = Utils.getImage(Res.IMG_BACKGROUND);
        this.imgBackground2 = Utils.getImage(Res.IMG_BACKGROUND);
        this.castle = Utils.getImage(Res.IMG_CASTLE);
        this.start = Utils.getImage(Res.START_ICON);

        this.mario = (Mario)this.characterFactory.createMario(MARIO_X,MARIO_Y);
        this.mushroom = (Mushroom)this.characterFactory.createMushroom(MUSHROOM_X,MUSHROOM_Y);
        this.turtle = (Turtle)this.characterFactory.createTurtle(TURTLE_X,TURTLE_Y);

        for (int i = 0; i < 8; i++) {
            Tunnel tunnel = new Tunnel(COORDS_X_TUNNEL[i],COORD_Y_TUNNEL);
            this.tunnelList.add(tunnel);
        }

        for (int j = 0; j < 12; j++) {
            Block block = new Block(COORDS_X_BLOCK[j],COORDS_Y_BLOCK[j]);
            this.blockList.add(block);
        }

        for (int m = 0; m < 10; m++) {
            Piece piece = new Piece(COORDS_X_PIECE[m],COORDS_Y_PIECE[m]);
            this.pieceList.add(piece);
        }

        this.imgCastle = Utils.getImage(Res.IMG_CASTLE_FINAL);
        this.imgFlag = Utils.getImage(Res.IMG_FLAG);

        objects = new ArrayList<>();

        this.objects.addAll(tunnelList);
        this.objects.addAll(blockList);

        pieces = new ArrayList<>();
        this.pieces.addAll(pieceList);

        this.setFocusable(true);
        this.requestFocusInWindow();

        this.addKeyListener(new Keyboard());
    }

    @Override
    public Mario getMario() {
        return this.mario;
    }

    @Override
    public Mushroom getMushroom() {
        return this.mushroom;
    }

    @Override
    public Turtle getTurtle() {
        return this.turtle;
    }

    @Override
    public int getFloorOffsetY() {
        return floorOffsetY;
    }

    @Override
    public int getHeightLimit() {
        return heightLimit;
    }

    @Override
    public int getMov() {
        return mov;
    }

    @Override
    public int getXPos() {
        return xPos;
    }

    @Override
    public void setBackground2PosX(int background2PosX) {
        this.background2PosX = background2PosX;
    }

    @Override
    public void setFloorOffsetY(int floorOffsetY) {
        this.floorOffsetY = floorOffsetY;
    }

    @Override
    public void setHeightLimit(int heightLimit) {
        this.heightLimit = heightLimit;
    }

    @Override
    public void setXPos(int xPos) {
        this.xPos = xPos;
    }

    @Override
    public void setMov(int mov) {
        this.mov = mov;
    }

    @Override
    public void setBackground1PosX(int x) {
        this.background1PosX = x;
    }

    @Override
    public void updateBackgroundOnMovement() {
        if (this.xPos >= 0 && this.xPos <= 4600) {
            this.xPos = this.xPos + this.mov;
            // Moving the screen to give the impression that Mario is walking
            this.background1PosX = this.background1PosX - this.mov;
            this.background2PosX = this.background2PosX - this.mov;
        }

        // Flipping between background1 and background2
        if (this.background1PosX == -800) {
            this.background1PosX = 800;
        }
        else if (this.background2PosX == -800) {
            this.background2PosX = 800;
        }
        else if (this.background1PosX == 800) {
            this.background1PosX = -800;
        }
        else if (this.background2PosX == 800) {
            this.background2PosX = -800;
        }
    }

    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        Graphics g2 = (Graphics2D) g;

        if (this.getXPos() == 4600) {
            new Thread(() -> {
                JOptionPane.showMessageDialog(PlatformImpl.getPlatform(), "YOU WIN!", "The end", JOptionPane.INFORMATION_MESSAGE);
                System.exit(0);
            }).start();
        }

        for (int i = 0; i < objects.size(); i++) {
            if (this.getMario().isNearby(this.objects.get(i)))
                this.getMario().contact(this.objects.get(i));

            if (this.getMushroom().isNearby(this.objects.get(i)))
                this.getMushroom().contact(this.objects.get(i));

            if (this.getTurtle().isNearby(this.objects.get(i)))
                this.getTurtle().contact(this.objects.get(i));
        }

        for (int i = 0; i < this.pieces.size(); i++) {
            if (this.getMario().contactPiece(this.pieces.get(i))) {
                Audio.playSound(Res.AUDIO_MONEY);
                this.pieces.remove(i);
            }
        }

        if (this.getMushroom().isNearby(turtle)) {
            this.getMushroom().contact(turtle);
        }
        if (this.getTurtle().isNearby(mushroom)) {
            this.getTurtle().contact(mushroom);
        }
        if (this.getMario().isNearby(mushroom)) {
            this.getMario().contact(mushroom);
        }
        if (this.getMario().isNearby(turtle)) {
            this.getMario().contact(turtle);
        }

        // Moving fixed objects
        this.updateBackgroundOnMovement();
        if (this.xPos >= 0 && this.xPos <= 4600) {
            for (int i = 0; i < objects.size(); i++) {
                objects.get(i).move();
            }

            for (int i = 0; i < pieces.size(); i++) {
                this.pieces.get(i).move();
            }

            this.getMushroom().move();
            this.getTurtle().move();
        }

        g2.drawImage(this.imgBackground1, this.background1PosX, 0, null);
        g2.drawImage(this.imgBackground2, this.background2PosX, 0, null);
        g2.drawImage(this.castle, 10 - this.xPos, 95, null);
        g2.drawImage(this.start, 220 - this.xPos, 234, null);

        for (int i = 0; i < objects.size(); i++) {
            g2.drawImage(this.objects.get(i).getImgObj(), this.objects.get(i).getX(),
                    this.objects.get(i).getY(), null);
        }

        for (int i = 0; i < pieces.size(); i++) {
            g2.drawImage(this.pieces.get(i).imageOnMovement(), this.pieces.get(i).getX(),
                    this.pieces.get(i).getY(), null);
        }

        g2.drawImage(this.imgFlag, FLAG_X_POS - this.xPos, FLAG_Y_POS, null);
        g2.drawImage(this.imgCastle, CASTLE_X_POS - this.xPos, CASTLE_Y_POS, null);

        if (this.getMario().isJumping())
            g2.drawImage(this.getMario().doJump(), this.getMario().getX(), this.getMario().getY(), null);
        else
            g2.drawImage(this.getMario().walk(Res.IMGP_CHARACTER_MARIO, MARIO_FREQUENCY), this.getMario().getX(), this.getMario().getY(), null);

        if (this.getMushroom().isAlive())
            g2.drawImage(this.getMushroom().walk(Res.IMGP_CHARACTER_MUSHROOM, MUSHROOM_FREQUENCY), this.getMushroom().getX(), this.getMushroom().getY(), null);
        else
            g2.drawImage(this.getMushroom().deadImage(), this.getMushroom().getX(), this.getMushroom().getY() + MUSHROOM_DEAD_OFFSET_Y, null);

        if (this.getTurtle().isAlive())
            g2.drawImage(this.getTurtle().walk(Res.IMGP_CHARACTER_TURTLE, TURTLE_FREQUENCY), this.getTurtle().getX(), this.getTurtle().getY(), null);
        else
            g2.drawImage(this.getTurtle().deadImage(), this.getTurtle().getX(), this.getTurtle().getY() + TURTLE_DEAD_OFFSET_Y, null);
    }
}
