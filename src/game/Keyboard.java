package game;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

public class Keyboard implements KeyListener {

    @Override
    public void keyPressed(KeyEvent e) {

        if (PlatformImpl.getPlatform().getMario().isAlive() == true) {
            if (e.getKeyCode() == KeyEvent.VK_RIGHT) {

                // per non fare muovere il castello e start
                if (PlatformImpl.getPlatform().getXPos() == -1) {
                    PlatformImpl.getPlatform().setXPos(0);
                    PlatformImpl.getPlatform().setBackground1PosX(-50);
                    PlatformImpl.getPlatform().setBackground2PosX(750);
                }
                PlatformImpl.getPlatform().getMario().setMoving(true);
                PlatformImpl.getPlatform().getMario().setToRight(true);
                PlatformImpl.getPlatform().setMov(1); // si muove verso sinistra
            } else if (e.getKeyCode() == KeyEvent.VK_LEFT) {

                if (PlatformImpl.getPlatform().getXPos() == 4601) {
                    PlatformImpl.getPlatform().setXPos(4600);
                    PlatformImpl.getPlatform().setBackground1PosX(-50);
                    PlatformImpl.getPlatform().setBackground2PosX(750);
                }

                PlatformImpl.getPlatform().getMario().setMoving(true);
                PlatformImpl.getPlatform().getMario().setToRight(false);
                PlatformImpl.getPlatform().setMov(-1); // si muove verso destra
            }
            // salto
            if (e.getKeyCode() == KeyEvent.VK_UP) {
                PlatformImpl.getPlatform().getMario().setJumping(true);
                Audio.playSound("/resources/audio/jump.wav");
            }

        }
    }

    @Override
    public void keyReleased(KeyEvent e) {
        PlatformImpl.getPlatform().getMario().setMoving(false);
        PlatformImpl.getPlatform().setMov(0);
    }

    @Override
    public void keyTyped(KeyEvent e) {
    }

}
