package game;

import characters.Mario;
import characters.Mushroom;
import characters.Turtle;
import interfaces.Character;
import interfaces.CharacterFactory;

/**
 * Created by Sofia Rosetti on 16/03/2017.
 */
public class CharacterFactoryImpl implements CharacterFactory{

    @Override
    public Character createMario(int x, int y) {
        return new Mario(x, y);
    }

    @Override
    public Character createMushroom(int x, int y) {
        return new Mushroom(x, y);
    }

    @Override
    public Character createTurtle(int x, int y) {
        return new Turtle(x, y);
    }
}
