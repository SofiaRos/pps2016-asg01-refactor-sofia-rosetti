package interfaces;

import characters.BasicCharacter;
import objects.GameObjectImpl;

import java.awt.Image;

public interface Character {

	int getX();

	int getY();

	int getWidth();

	int getHeight();

	int getCounter();

	boolean isAlive();

	boolean isMoving();

	boolean isToRight();

	void setAlive(boolean alive);

	void setX(int x);

	void setY(int y);

	void setMoving(boolean moving);

	void setToRight(boolean toRight);

	void setCounter(int counter);

	Image walk(String name, int frequency);

	void move();

	boolean hitAhead(GameObjectImpl og);

	boolean hitBelow(BasicCharacter pers);

	boolean isNearby(BasicCharacter pers);

	boolean isNearby(GameObjectImpl obj);
}
