package interfaces;

/**
 * Created by Sofia Rosetti on 16/03/2017.
 */
public interface CharacterFactory {

    Character createMario(int x, int y);

    Character createMushroom(int x, int y);

    Character createTurtle(int x, int y);

}
