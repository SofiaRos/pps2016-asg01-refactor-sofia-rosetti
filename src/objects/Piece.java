package objects;

import utils.Res;
import utils.Utils;

import java.awt.Image;

public class Piece extends GameObjectImpl implements Runnable {

    private static final int WIDTH = 30;
    private static final int HEIGHT = 30;
    private static final int PAUSE = 10;
    private static final int FLIP_FREQUENCY = 100;
    private int counter;

    public Piece(int x, int y) {
        super(x, y, WIDTH, HEIGHT);
        super.imgObj = Utils.getImage(Res.IMG_PIECE1);
    }

    public Image imageOnMovement() {
        return Utils.getImage(++this.counter % FLIP_FREQUENCY == 0 ? Res.IMG_PIECE1 : Res.IMG_PIECE2);
    }

    @Override
    public void run() {
        while (true) {
            this.imageOnMovement();
            try {
                Thread.sleep(PAUSE);
            } catch (InterruptedException e) {
            }
        }
    }

}
