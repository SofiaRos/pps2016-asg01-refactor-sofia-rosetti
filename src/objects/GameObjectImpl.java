package objects;

import java.awt.Image;

import game.PlatformImpl;
import interfaces.GameObject;

public class GameObjectImpl implements GameObject {

    private int width, height;
    private int x, y;

    protected Image imgObj;

    public GameObjectImpl(int x, int y, int width, int height) {
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
    }

    @Override
    public int getWidth() {
        return width;
    }

    @Override
    public int getHeight() {
        return height;
    }

    @Override
    public int getX() {
        return x;
    }

    @Override
    public int getY() {
        return y;
    }

    @Override
    public Image getImgObj() {
        return imgObj;
    }

    @Override
    public void setWidth(int width) {
        this.width = width;
    }

    @Override
    public void setHeight(int height) {
        this.height = height;
    }

    @Override
    public void setX(int x) {
        this.x = x;
    }

    @Override
    public void setY(int y) {
        this.y = y;
    }

    @Override
    public void move() {
        if (PlatformImpl.getPlatform().getXPos() >= 0) {
            this.x = this.x - PlatformImpl.getPlatform().getMov();
        }
    }
}
