package objects;

import utils.Res;
import utils.Utils;

public class Block extends GameObjectImpl {

    private static final int WIDTH = 30;
    private static final int HEIGHT = 30;

    public Block(int x, int y) {
        super(x, y, WIDTH, HEIGHT);
        super.imgObj = Utils.getImage(Res.IMG_BLOCK);
    }

}
