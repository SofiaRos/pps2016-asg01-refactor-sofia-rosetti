package characters;

import java.awt.Image;

import game.PlatformImpl;
import objects.GameObjectImpl;
import objects.Piece;
import utils.Res;
import utils.Utils;

public class Mario extends BasicCharacter {

    private static final int MARIO_OFFSET_Y_INITIAL = 243;
    private static final int FLOOR_OFFSET_Y_INITIAL = 293;
    private static final int WIDTH = 28;
    private static final int HEIGHT = 50;
    private static final int JUMPING_LIMIT = 42;

    private Image imgMario;
    private boolean jumping;
    private int jumpingExtent;

    public Mario(int x, int y) {
        super(x, y, WIDTH, HEIGHT);
        this.imgMario = Utils.getImage(Res.IMG_MARIO_DEFAULT);
        this.jumping = false;
        this.jumpingExtent = 0;
    }

    public boolean isJumping() {
        return this.jumping;
    }

    public void setJumping(boolean jumping) {
        this.jumping = jumping;
    }

    public Image doJump() {
        String str;

        this.jumpingExtent++;

        if (this.jumpingExtent < JUMPING_LIMIT) {
            if (this.getY() > PlatformImpl.getPlatform().getHeightLimit()) {
                this.setY(this.getY() - 4);
            } else {
                this.jumpingExtent = JUMPING_LIMIT;
            }
            str = this.isToRight() ? Res.IMG_MARIO_SUPER_DX : Res.IMG_MARIO_SUPER_SX;
        } else if (this.getY() + this.getHeight() < PlatformImpl.getPlatform().getFloorOffsetY()) {
            this.setY(this.getY() + 1);
            str = this.isToRight() ? Res.IMG_MARIO_SUPER_DX : Res.IMG_MARIO_SUPER_SX;
        } else {
            str = this.isToRight() ? Res.IMG_MARIO_ACTIVE_DX : Res.IMG_MARIO_ACTIVE_SX;
            this.jumping = false;
            this.jumpingExtent = 0;
        }
        return Utils.getImage(str);
    }

    public boolean contactPiece(Piece piece) {
        if (this.hitBack(piece) || this.hitAbove(piece) || this.hitAhead(piece)
                || this.hitBelow(piece)) {
            return true;
        }
        return false;
    }

    public void contact(GameObjectImpl obj) {
        if (this.hitAhead(obj) && this.isToRight() || this.hitBack(obj) && !this.isToRight()) {
            PlatformImpl.getPlatform().setMov(0);
            this.setMoving(false);
        }

        if (this.hitBelow(obj) && this.isJumping()) {
            PlatformImpl.getPlatform().setFloorOffsetY(obj.getY());
        } else if (!this.hitBelow(obj)) {
            PlatformImpl.getPlatform().setFloorOffsetY(FLOOR_OFFSET_Y_INITIAL);
            if (!this.jumping) {
                this.setY(MARIO_OFFSET_Y_INITIAL);
            }

            if (hitAbove(obj)) {
                PlatformImpl.getPlatform().setHeightLimit(obj.getY() + obj.getHeight()); // the new sky goes below the object
            } else if (!this.hitAbove(obj) && !this.jumping) {
                PlatformImpl.getPlatform().setHeightLimit(0); // initial sky
            }
        }
    }

    public void contact(BasicCharacter pers) {
        if (this.hitAhead(pers) || this.hitBack(pers)) {
            if (pers.isAlive()) {
                this.setMoving(false);
                this.setAlive(false);
            } else this.setAlive(true);
        } else if (this.hitBelow(pers)) {
            pers.setMoving(false);
            pers.setAlive(false);
        }
    }
}
