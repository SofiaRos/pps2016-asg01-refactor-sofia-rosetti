package characters;

import java.awt.Image;

import objects.GameObjectImpl;
import utils.Res;
import utils.Utils;

public class Mushroom extends BasicCharacter implements Runnable {

    private static final int WIDTH = 27;
    private static final int HEIGHT = 30;

    private Image img;
    private final int PAUSE = 15;

    private int offsetX;

    public Mushroom(int x, int y) {
        super(x, y, WIDTH, HEIGHT);
        this.setToRight(true);
        this.setMoving(true);
        this.offsetX = 1;
        this.img = Utils.getImage(Res.IMG_MUSHROOM_DEFAULT);

        Thread chronoMushroom = new Thread(this);
        chronoMushroom.start();
    }

    //getters
    public Image getImg() {
        return img;
    }

    public int getOffsetX() {
        return offsetX;
    }

    public void setOffsetX(int offsetX) {
        this.offsetX = offsetX;
    }

    public void move() {
        this.setOffsetX(isToRight() ? 1 : -1);
        this.setX(this.getX() + this.getOffsetX());

    }

    @Override
    public void run() {
        while (true) {
            if (this.isAlive() == true) {
                this.move();
                try {
                    Thread.sleep(PAUSE);
                } catch (InterruptedException e) {
                }
            }
        }
    }

    public Image deadImage() {
        return Utils.getImage(this.isToRight() ? Res.IMG_MUSHROOM_DEAD_DX : Res.IMG_MUSHROOM_DEAD_SX);
    }

    public void contact(GameObjectImpl obj) {
        if (this.hitAhead(obj) && this.isToRight()) {
            this.setToRight(false);
            this.setOffsetX(-1);
        } else if (this.hitBack(obj) && !this.isToRight()) {
            this.setToRight(true);
            this.setOffsetX(1);
        }
    }

    public void contact(BasicCharacter pers) {
        if (this.hitAhead(pers) && this.isToRight()) {
            this.setToRight(false);
            this.setOffsetX(-1);
        } else if (this.hitBack(pers) && !this.isToRight()) {
            this.setToRight(true);
            this.setOffsetX(1);
        }
    }
}
