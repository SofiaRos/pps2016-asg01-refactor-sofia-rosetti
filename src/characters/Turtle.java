package characters;

import java.awt.Image;
import objects.GameObjectImpl;
import utils.Res;
import utils.Utils;

public class Turtle extends BasicCharacter implements Runnable {

    private static final int WIDTH = 43;
    private static final int HEIGHT = 50;

    private Image imgTurtle;
    private final int PAUSE = 15;

    private int dxTurtle;

    public Turtle(int X, int Y) {
        super(X, Y, WIDTH, HEIGHT);
        super.setToRight(true);
        super.setMoving(true);
        this.dxTurtle = 1;
        this.imgTurtle = Utils.getImage(Res.IMG_TURTLE_IDLE);

        Thread chronoTurtle = new Thread(this);
        chronoTurtle.start();
    }

    public int getDxTurtle() {
        return this.dxTurtle;
    }

    public void setDxTurtle(int dxTurtle) {
        this.dxTurtle = dxTurtle;
    }

    public Image getImgTurtle() {
        return this.imgTurtle;
    }

    public void move() {
        this.setDxTurtle(isToRight() ? 1 : -1);
        super.setX(super.getX() + this.getDxTurtle());
    }

    @Override
    public void run() {
        while (true) {
            if (this.isAlive()) {
                this.move();
                try {
                    Thread.sleep(PAUSE);
                } catch (InterruptedException e) {
                }
            }
        }
    }

    public Image deadImage() {
        return Utils.getImage(Res.IMG_TURTLE_DEAD);
    }

    public void contact(GameObjectImpl obj) {
        if (this.hitAhead(obj) && this.isToRight()) {
            this.setToRight(false);
            this.setDxTurtle(-1);
        } else if (this.hitBack(obj) && !this.isToRight()) {
            this.setToRight(true);
            this.setDxTurtle(1);
        }
    }

    public void contact(BasicCharacter pers) {
        if (this.hitAhead(pers) == true && this.isToRight() == true) {
            this.setToRight(false);
            this.setDxTurtle(-1);
        } else if (this.hitBack(pers) == true && this.isToRight() == false) {
            this.setToRight(true);
            this.setDxTurtle(1);
        }
    }
}
