package characters;

import java.awt.Image;

import game.PlatformImpl;
import interfaces.Character;
import objects.GameObjectImpl;
import utils.Res;
import utils.Utils;

public class BasicCharacter implements Character {

    private static final int PROXIMITY_MARGIN = 10;
    private static final int HIT_MARGIN = 5;
    private int width, height;
    private int x, y;
    private boolean moving;
    private boolean toRight;
    private int counter;
    private boolean alive;

    public BasicCharacter(int x, int y, int width, int height) {
        this.x = x;
        this.y = y;
        this.height = height;
        this.width = width;
        this.counter = 0;
        this.moving = false;
        this.toRight = true;
        this.alive = true;
    }

    @Override
    public int getX() {
        return this.x;
    }

    @Override
    public int getY() {
        return this.y;
    }

    @Override
    public int getWidth() {
        return this.width;
    }

    @Override
    public int getHeight() {
        return this.height;
    }

    @Override
    public int getCounter() {
        return this.counter;
    }

    @Override
    public boolean isAlive() {
        return alive;
    }

    @Override
    public boolean isMoving() {
        return moving;
    }

    @Override
    public boolean isToRight() {
        return toRight;
    }

    @Override
    public void setAlive(boolean alive) {
        this.alive = alive;
    }

    @Override
    public void setX(int x) {
        this.x = x;
    }

    @Override
    public void setY(int y) {
        this.y = y;
    }

    @Override
    public void setMoving(boolean moving) {
        this.moving = moving;
    }

    @Override
    public void setToRight(boolean toRight) {
        this.toRight = toRight;
    }

    @Override
    public void setCounter(int counter) {
        this.counter = counter;
    }

    @Override
    public Image walk(String name, int frequency) {
        String str = Res.IMG_BASE + name + (!this.moving || ++this.counter % frequency == 0 ? Res.IMGP_STATUS_ACTIVE : Res.IMGP_STATUS_NORMAL) +
                (this.toRight ? Res.IMGP_DIRECTION_DX : Res.IMGP_DIRECTION_SX) + Res.IMG_EXT;
        return Utils.getImage(str);
    }

    @Override
    public void move() {
        if (PlatformImpl.getPlatform().getXPos() >= 0) {
            this.x = this.x - PlatformImpl.getPlatform().getMov();
        }
    }

    @Override
    public boolean hitAhead(GameObjectImpl og) {
        return !(this.x + this.width < og.getX() || this.x + this.width > og.getX() + HIT_MARGIN ||
                this.y + this.height <= og.getY() || this.y >= og.getY() + og.getHeight());
    }

    protected boolean hitBack(GameObjectImpl og) {
        return !(this.x > og.getX() + og.getWidth() || this.x + this.width < og.getX() + og.getWidth() - HIT_MARGIN ||
                this.y + this.height <= og.getY() || this.y >= og.getY() + og.getHeight());
    }

    protected boolean hitBelow(GameObjectImpl og) {
        return !(this.x + this.width < og.getX() + HIT_MARGIN || this.x > og.getX() + og.getWidth() - HIT_MARGIN ||
                this.y + this.height < og.getY() || this.y + this.height > og.getY() + 5);
    }

    protected boolean hitAbove(GameObjectImpl og) {
        return !(this.x + this.width < og.getX() + HIT_MARGIN || this.x > og.getX() + og.getWidth() - HIT_MARGIN ||
                this.y < og.getY() + og.getHeight() || this.y > og.getY() + og.getHeight() + HIT_MARGIN);

    }

    protected boolean hitAhead(BasicCharacter pers) {
        if (this.isToRight() == true) {
            return !(this.x + this.width < pers.getX() || this.x + this.width > pers.getX() + HIT_MARGIN ||
                    this.y + this.height <= pers.getY() || this.y >= pers.getY() + pers.getHeight());
        } else {
            return false;
        }
    }

    protected boolean hitBack(BasicCharacter pers) {
        return !(this.x > pers.getX() + pers.getWidth() || this.x + this.width < pers.getX() + pers.getWidth() - HIT_MARGIN ||
                this.y + this.height <= pers.getY() || this.y >= pers.getY() + pers.getHeight());

    }

    @Override
    public boolean hitBelow(BasicCharacter pers) {
        return !(this.x + this.width < pers.getX() || this.x > pers.getX() + pers.getWidth() ||
                this.y + this.height < pers.getY() || this.y + this.height > pers.getY());

    }

    @Override
    public boolean isNearby(BasicCharacter pers) {
        return ((this.x > pers.getX() - PROXIMITY_MARGIN && this.x < pers.getX() + pers.getWidth() + PROXIMITY_MARGIN)
                || (this.x + this.width > pers.getX() - PROXIMITY_MARGIN && this.x + this.width < pers.getX() + pers.getWidth() + PROXIMITY_MARGIN));

    }

    @Override
    public boolean isNearby(GameObjectImpl obj) {
        return ((this.x > obj.getX() - PROXIMITY_MARGIN && this.x < obj.getX() + obj.getWidth() + PROXIMITY_MARGIN) ||
                (this.getX() + this.width > obj.getX() - PROXIMITY_MARGIN && this.x + this.width < obj.getX() + obj.getWidth() + PROXIMITY_MARGIN));
    }

}
