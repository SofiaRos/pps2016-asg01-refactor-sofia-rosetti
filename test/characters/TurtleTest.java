package characters;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by Sofia Rosetti on 16/03/2017.
 */
public class TurtleTest {

    BasicCharacter turtle;

    @Before
    public void initialize() {
        this.turtle = new Turtle(20,0);
    }

    @Test
    public void runTest() throws Exception {
        turtle.setAlive(true);
        assertTrue(turtle.isMoving());
    }

}