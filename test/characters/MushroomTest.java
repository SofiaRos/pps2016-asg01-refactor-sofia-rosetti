package characters;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by Sofia Rosetti on 16/03/2017.
 */
public class MushroomTest {

    BasicCharacter mushroom;

    @Before
    public void initialize() {
        this.mushroom = new Mushroom(20,0);
    }

    @Test
    public void runTest() throws Exception {
        mushroom.setAlive(true);
        assertTrue(mushroom.isMoving());
    }

}