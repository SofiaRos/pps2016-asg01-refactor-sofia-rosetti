package characters;

import objects.GameObjectImpl;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by Sofia Rosetti on 16/03/2017.
 */
public class BasicCharacterTest {

    BasicCharacter bc;
    GameObjectImpl goi;
    BasicCharacter turtle;
    BasicCharacter mushroom;

    @Before
    public void initialize() {
        this.bc = new BasicCharacter(5,0,10,15);
        this.goi = new GameObjectImpl(15,0,10,10);
        this.turtle = new Turtle(10,0);
        this.mushroom = new Mushroom(20,0);
    }

    @Test
    public void hitAheadTestGoi() throws Exception {
        assertTrue(bc.hitAhead(goi));
    }

    @Test
    public void hitBackTestGoi() throws Exception {
        this.bc.setX(25);
        assertTrue(bc.hitBack(goi));
    }

    @Test
    public void hitBelowTestGoi() throws Exception {
        assertFalse(bc.hitBelow(goi));
    }

    @Test
    public void hitAboveTest() throws Exception {
        assertFalse(bc.hitAbove(goi));
    }

    @Test
    public void hitAheadTestBc() throws Exception {
        bc.setToRight(false);
        assertFalse(bc.hitAhead(turtle));
        bc.setToRight(true);
        assertTrue(bc.hitAhead(turtle));
    }

    @Test
    public void hitBackTestBc() throws Exception {
        assertFalse(bc.hitBack(mushroom));
    }

    @Test
    public void hitBelowTestBc() throws Exception {
        assertFalse(bc.hitBelow(turtle));
    }

    @Test
    public void isNearbyTestGoi() throws Exception {
        assertTrue(bc.isNearby(goi));
    }

    @Test
    public void isNearbyTestBc() throws Exception {
        assertTrue(bc.isNearby(mushroom));
    }

}